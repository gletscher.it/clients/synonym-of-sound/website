(function () {
    const colorThief = new ColorThief();
    let projectData = [];
    let projects = document.getElementsByClassName("projects")[0];
  
    /**
     * Converts RGB color to HSL (http://en.wikipedia.org/wiki/HSL_color_space)
     * @param   Number Red
     * @param   Number Green
     * @param   Number Blue
     * @returns Array  HSL (deg, %, %, a)
     */
    function rgbToHsl([r, g, b, a = 1]) {
      r /= 255, g /= 255, b /= 255;
  
      let max = Math.max(r, g, b);
      let min = Math.min(r, g, b);
      let h, s, l = (max + min) / 2;
  
      if (max === min) {
        h = s = 0; // achromatic
      }
      else {
        let d = max - min;
        s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
  
        switch (max) {
          case r:
            h = (g - b) / d + (g < b ? 6 : 0);
            break;
          case g:
            h = (b - r) / d + 2;
            break;
          case b:
            h = (r - g) / d + 4;
            break;
        }
      }
  
      return [Math.round(h * 60), Math.round(100 * s), Math.round(100 * l), a];
    }
  
    /**
     * Creates a `.project` element with more information about a project
     * @param Object DOM element
     * @param Object Project data
     */
    function expandProject(el, data) {
      let f = el.classList.contains("cover--selected");
      
      // Close all existing project panels
      for (const project of document.getElementsByClassName("project")) {
        projects.removeChild(project);
      }
      for (const cover of document.getElementsByClassName("cover")) {
        cover.classList.remove("cover--selected");
      }
  
      if (f) {
        return;
      }
  
      // Create root element
      let project = document.createElement("div");
      project.classList.add("project");
  
      // Create heading
      let heading = document.createElement("h3");
      heading.classList.add("project__heading");
      heading.innerHTML = `<a href="${data.link}" target="_blank"><span class="project__heading__title">${data.title}</span> <span class="project__heading__artist">~ ${data.artist}</span><i class="project__heading__ext ri-external-link-line"></i></a></h3>`;
  
      // Create credits
      let credits = document.createElement("div");
      credits.classList.add("project__credits");
      for (const c of data.credits.credits) {
        let p = document.createElement("p");
        p.innerHTML = c;
        credits.appendChild(p);
      }
  
      // Create YouTube embed
      let yt = document.createElement("iframe");
      yt.classList.add("project__video");
      yt.src = "https://www.youtube-nocookie.com/embed/" + data.embed.url;
      yt.frameBorder = 0;
      yt.allow = "accelerometer; clipboard-write; encrypted-media; gyroscope";
      yt.allowFullscreen = true;
  
      // Append children to root element
      project.appendChild(heading);
      project.appendChild(credits);
      project.appendChild(yt);
  
      // Set color
      let c = el.getAttribute("data-hsla").split(",");
      console.log(c);
      // project.style.backgroundColor = `hsl(${c[0]}, ${c[1]}%, 35%)`;
  
      // Add visual extension to cover
      el.classList.add("cover--selected");
  
      // Append project to DOM
      projects.insertBefore(project, el.nextSibling);
    }
  
    /**
     * Loads projects from JSON file and populates page with covers
     */
    async function loadContent() {
      await fetch("assets/projects.json")
        .then(response => response.json())
        .then(data => {
          projectData = data;
          projects.innerHTML = "";
          for (const project of data) {
            // Create root element
            let a = document.createElement("a");
            a.className = "cover";
            a.href = `#${project.date}+${project.artist}+${project.title}`
  
            // Create cover
            let img = document.createElement("img");
            if (project.cover) {
              img.src = project.cover;
            }
            else {
              img.src = `/assets/covers/${project.artist} - ${project.title}.392.jpg`;
            }
            img.alt = project.artist + " - " + project.title;
  
            // Create caption
            let cap = document.createElement("div");
            cap.classList.add("cover__caption");
            let h3 = document.createElement("h3");
            h3.classList.add("cover__caption__title");
            h3.textContent = project.title;
            let h4 = document.createElement("h4");
            h4.classList.add("cover__caption__artist");
            h4.textContent = project.artist;
            cap.appendChild(h3);
            cap.appendChild(h4);
  
            // Append cover & caption
            a.appendChild(img);
            a.appendChild(cap);
  
            // Set colors
            function setColors(img) {
              let c = rgbToHsl(colorThief.getColor(img));
              a.setAttribute("data-hsla", c);
              // a.style.backgroundColor = `hsl(${c[0]}, ${c[1]}%, 35%)`;
              // cap.style.backgroundColor = `hsla(${c[0]}, ${c[1]}%, 35%, .9)`;
            }
            img.addEventListener('load', function () {
              setColors(img);
            });
  
            a.addEventListener("click", function (ev) {
              expandProject(this, project);
            });
  
            // Append to DOM
            projects.appendChild(a);
          }
        });
      console.log("foo");
    }
  
    loadContent();
    console.log("bar");
  })();
  